package com.sky.entity;


import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ContractDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 合同主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 合同编号
     */
    private Integer contractCode;

    /**
     * 设备质保期
     */
    private String warranty;

    /**
     * 服务时间
     */
    private Date serviceTime;

    /**
     * 客户名称
     */
    private String custom;

    /**
     * 客户电话
     */
    private String customPhone;

    /**
     * 服务地点
     */
    private String address;

    /**
     * 服务联系人
     */
    private String contacts;

    /**
     * 销售经理
     */
    private String sale;

    /**
     * 服务人员
     */
    private String staff;

    /**
     * 备注
     */
    private String remark;

    /**
     * 合同id
     */
    private Integer contractId;

}

package com.sky.entity;


import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Contract implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 合同id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 合同编码
     */
    private Integer contractCode;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 区域
     */
    private String area;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态:0否，1是  是否过期
     */
    private Boolean state;


}

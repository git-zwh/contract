package com.sky.dto;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.sky.entity.ContractDetails;
import lombok.Data;

import java.io.Serializable;

@Data
public class ContractDTO implements Serializable {

    private ContractDetails contractDetails ;

    /**
     * 合同id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 合同编码
     */
    private Integer contractCode;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 区域
     */
    private String area;

    /**
     * 备注
     */
    private String remark;

}

package com.sky.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class MessageDTO implements Serializable {

    //合同编号
    private String contractName;

    //页码
    private int page;

    //每页显示记录数
    private int pageSize;
}

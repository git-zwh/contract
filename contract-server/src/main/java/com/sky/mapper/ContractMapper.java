package com.sky.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.pagehelper.Page;
import com.sky.dto.ContractPageDTO;
import com.sky.entity.Contract;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@Mapper
public interface ContractMapper extends BaseMapper<Contract> {

    Page<Contract> pageQuery(ContractPageDTO contractDTO);

    @Select("select contract_code from contract where id=#{id} ;")
    Long selectContractCode(Long id);

    @Insert("insert into contract (contract_code, contract_name, area, remark, state) " +
            "VALUE  "+"(#{contractCode},#{contractName},#{area},#{remark},#{state})")
    Integer insert(Contract contract);

    @Delete("delete from contract where id=#{id};")
    void deleteById(Long id);

    @Select("select * from contract where id=#{id}")
    Contract selectId(Long id);

    void update(Contract contract);
}

package com.sky.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.sky.entity.ContractDetails;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@Mapper
public interface ContractDetailsMapper extends BaseMapper<ContractDetails> {

    @Delete("delete from contract_details where contract_code = #{id}")
    void deleteByContractDetailsId(Long id);


    @Insert("insert into contract_details (contract_code, warranty, service_time, custom, custom_phone, address, contacts, sale, staff, remark)" +
            "VALUE  "+"(#{contractCode},#{warranty},#{serviceTime},#{custom},#{customPhone},#{address},#{contacts},#{sale},#{staff},#{remark})")
    Integer insert(ContractDetails contractDetails);

    void update(ContractDetails contractDetails);

    @Select("select * from contract_details where contract_id = #{id}")
    ContractDetails selectByContractId(Long id);
}

package com.sky.controller;


import com.sky.dto.ContractDTO;
import com.sky.dto.ContractPageDTO;
import com.sky.dto.MessageDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.ContractService;
import com.sky.service.MessageService;
import com.sky.vo.ContractVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2023-12-07
 */
@Slf4j
@RestController
@RequestMapping("/message")
@Api(tags = "消息相关接口")
public class MessageController {

    @Autowired
    private ContractService contractService;
    @Autowired
    private MessageService messageService;

    /**
     * 分页查询
     * @param contractDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("消息分页")
    public Result<PageResult> page(MessageDTO messageDTO){
        log.info("员工分页查询，参数为：{}", messageDTO);
        PageResult pageResult = messageService.pageQuery(messageDTO);
        return Result.success(pageResult);
    }



    /**
     * 合同批量删除
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("合同 合同信息批量删除")
    public Result delete(@RequestParam List<Long> ids) {
        log.info("合同批量删除：{}", ids);
        contractService.deleteContract(ids);

        return Result.success();
    }







}


package com.sky.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@RestController
@RequestMapping("/contract-details")
public class ContractDetailsController {

}


package com.sky.controller;


import com.sky.dto.ContractDTO;
import com.sky.dto.ContractPageDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.ContractService;
import com.sky.vo.ContractVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@Slf4j
@RestController
@Api(tags = "合同信息")
@RequestMapping("/contract")
public class ContractController {

    @Autowired
    private ContractService contractService;

    /**
     * 分页查询
     * @param contractDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("合同分页")
    public Result<PageResult> page(ContractPageDTO contractDTO){
        log.info("员工分页查询，参数为：{}", contractDTO);
        PageResult pageResult = contractService.pageQuery(contractDTO);
        return Result.success(pageResult);
    }

    /**
     * 新增合同
     * @param contractDTO
     * @return
     */
    @PostMapping
    @ApiOperation("新增合同 合同信息")
    public Result add(@RequestBody ContractDTO contractDTO){
        log.info("新增合同：{}", contractDTO);
        contractService.saveContract(contractDTO);
        return Result.success();
    }

    /**
     * 合同批量删除
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("合同 合同信息批量删除")
    public Result delete(@RequestParam List<Long> ids) {
        log.info("合同批量删除：{}", ids);
        contractService.deleteContract(ids);

        return Result.success();
    }

    /**
     * 修改合同 合同信息
     * @param contractDTO
     * @return
     */
    @ApiOperation("修改合同 合同信息")
    @PutMapping
    public Result update(@RequestBody ContractDTO contractDTO) {
        contractService.update(contractDTO);
        return Result.success();
    }


    @ApiOperation("根据id查询合同 合同信息")
    @GetMapping("/{id}")
    public Result selectId(@PathVariable Long id) {
        ContractVO contractVO=contractService.selectById(id);

        return Result.success(contractVO);
    }



}


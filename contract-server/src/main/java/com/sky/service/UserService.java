package com.sky.service;


import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2023-12-05
 */
public interface UserService {



    /**
     * 用户登录
     * @param userLoginDTO
     * @return
     */
    User login(UserLoginDTO userLoginDTO);

    /**
     * 新增用户
     * @param user
     */
    void sava(User user);
}

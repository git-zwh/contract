package com.sky.service;


import com.baomidou.mybatisplus.service.IService;
import com.sky.dto.ContractDTO;
import com.sky.dto.ContractPageDTO;
import com.sky.entity.Contract;
import com.sky.result.PageResult;
import com.sky.vo.ContractVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
public interface ContractService extends IService<Contract> {

    PageResult pageQuery(ContractPageDTO contractDTO);


    void deleteContract(List<Long> ids);

    void saveContract(ContractDTO contractDTO);


    void update(ContractDTO contractDTO);
    /**
     * 根据id查合同 合同信息
     * @param id
     * @return
     */
    ContractVO selectById(Long id);
}

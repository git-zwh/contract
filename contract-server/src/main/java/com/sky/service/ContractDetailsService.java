package com.sky.service;


import com.baomidou.mybatisplus.service.IService;
import com.sky.entity.ContractDetails;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
public interface ContractDetailsService extends IService<ContractDetails> {

}

package com.sky.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.dto.ContractDTO;
import com.sky.dto.ContractPageDTO;
import com.sky.entity.Contract;
import com.sky.entity.ContractDetails;
import com.sky.exception.LoginFailedException;
import com.sky.mapper.ContractDetailsMapper;
import com.sky.mapper.ContractMapper;
import com.sky.result.PageResult;
import com.sky.service.ContractService;
import com.sky.vo.ContractVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@Slf4j
@Service
public class ContractServiceImpl extends ServiceImpl<ContractMapper, Contract> implements ContractService {

    @Autowired
    private ContractMapper contractMapper;
    @Autowired
    private ContractDetailsMapper contractDetailsMapper;

    /**
     * 分页查询
     * @param contractDTO
     * @return
     */
    @Override
    public PageResult pageQuery(ContractPageDTO contractDTO) {
        //开始分页查询
        PageHelper.startPage(contractDTO.getPage(), contractDTO.getPageSize());
        Page<Contract> page = contractMapper.pageQuery(contractDTO);

        long total = page.getTotal();
        List<Contract> records = page.getResult();

        return new PageResult(total, records);
    }

    /**
     * 删除合同 合同信息
     * @param ids
     */
    @Override
    public void deleteContract(List<Long> ids) {
        //删除合同表中的合同数据
        for (Long id : ids) {
            //查询合同编码
            Long contractCode = contractMapper.selectContractCode(id);
            //删除合同关联的合同信息数据
            contractDetailsMapper.deleteByContractDetailsId(contractCode);
            contractMapper.deleteById(id);
        }
    }

    /**
     * 添加合同信息
     * @param contractDTO
     */
    @Override
    public void saveContract(ContractDTO contractDTO) {

        Contract contract = new Contract();
//拷贝合同
        BeanUtils.copyProperties(contractDTO, contract);
        //赋值合同信息
        ContractDetails contractDetails = contractDTO.getContractDetails();
        //同步合同编号
        contractDetails.setContractCode(contract.getContractCode());
        contractDetails.setContractId(contract.getId());
        //向合同表插入1条数据
        contractMapper.insert(contract);

        contractDetailsMapper.insert(contractDTO.getContractDetails());
    }

    /**
     * 更新合同 合同信息
     * @param contractDTO
     */
    @Override
    public void update(ContractDTO contractDTO) {
        Contract contract = new Contract();
        BeanUtils.copyProperties(contractDTO, contract);

        ContractDetails contractDetails = contractDTO.getContractDetails();
        //合同信息获取合同编号
        contractDetails.setContractCode(contract.getContractCode());
        //修改合同信息
        contractDetailsMapper.update(contractDetails);

        //修改合同
        contractMapper.update(contract);


    }

    /**
     * 根据id查合同 合同信息
     * @param id
     * @return
     */
    @Override
    public ContractVO selectById(Long id) {
        ContractVO contractVO = new ContractVO();

        Contract contract = contractMapper.selectId(id);

        BeanUtils.copyProperties(contract, contractVO);

        ContractDetails contractDetails = contractDetailsMapper.selectByContractId(id);
        log.info("新增用户：{}", contractDetails);
        contractVO.setContractDetails(contractDetails);
        
        log.info("新增用户：{}", contractVO);

        return contractVO;
    }


}

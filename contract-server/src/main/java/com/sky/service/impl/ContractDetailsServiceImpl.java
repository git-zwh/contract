package com.sky.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.sky.entity.ContractDetails;
import com.sky.mapper.ContractDetailsMapper;
import com.sky.service.ContractDetailsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2023-12-06
 */
@Service
public class ContractDetailsServiceImpl extends ServiceImpl<ContractDetailsMapper, ContractDetails> implements ContractDetailsService {

}

package com.sky.service;


import com.baomidou.mybatisplus.service.IService;
import com.sky.dto.MessageDTO;
import com.sky.entity.Message;
import com.sky.result.PageResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2023-12-07
 */
public interface MessageService extends IService<Message> {

    PageResult pageQuery(MessageDTO messageDTO);
}
